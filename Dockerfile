FROM mambaorg/micromamba:0.11.3
LABEL authors="huether@bio.lmu.de" \
      description="Container image containing all dependencies for the gwas-nf pipeline"

COPY environment.yml /environment.yml

RUN micromamba install -y -n base -f /environment.yml && micromamba clean -a
